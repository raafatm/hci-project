import os
import pandas as pd
import re

columns = ["curl_start", "curl_end", "rsync_start", "rsync_end", "group"]

df = pd.DataFrame(columns=columns)
df.index.name = "user"
for r, d, files in os.walk("/srv/timing"):
    for f in files:
        filepath = r + os.sep + f
        user = filepath.split(os.sep)[-2]
        user_num = int(re.sub('[^0-9]', '', user))
        if user_num < 50:
            user_group = "TAB"
        elif user_num < 100:
            user_group = "BASE"
        else:
            user_group = "TUI"

        task = filepath.split(os.sep)[-1]
        with open(filepath) as reader:
            string_times = reader.readlines()
            times = [int(st.strip()) for st in string_times]
            time = min(times)
            df.at[user, task] = time
            df.at[user, columns[4]] = user_group

ct = df[columns[1]]-df[columns[0]]
rt = df[columns[3]]-df[columns[2]]

cf = ct.to_frame()
cf.columns = ["curl"]
rf = rt.to_frame()
rf.columns = ["rsync"]
sdf = cf.join(rf, on="user")
df = df.join(sdf, on="user")

df[columns[4]] = df[columns[4]].astype('category')
#print(df)
print(df.to_csv())

import subprocess
import os
import pandas as pd
import numpy as np
from os import path

fsizes = {
    "lovelace.json": 1043,
    "thoreau.json": 13337,
    "thomas": 12,
    "bronte.json": 63977
}

CURL_FILE_NAME = "curl_response.html"
df = pd.read_csv("data.csv")
users = df["user"]
df = df.set_index("user")
df["curl_completed"] = False
df["curl_completed_file_creation_time"] = np.nan
df["rsync_completed"] = False
df["rsync_completed_file_creation_time"] = np.nan

for user in users:
    homedir = path.join(path.abspath(os.sep), "home",  user)
    curl_file_path = path.join(homedir, CURL_FILE_NAME)
    curl_completed = False
    if path.exists(curl_file_path):
        with open(curl_file_path) as creader:
            ctext = creader.read()
            if "pineapple" in ctext:
                curl_mtime = path.getmtime(curl_file_path)
                curl_completed = True
    df.at[user, "curl_completed"] = curl_completed
    if curl_completed:
        df.at[user, "curl_completed_file_creation_time"] = curl_mtime

    rsync_completed = True
    rsync_mtime = 0
    dest_dir = path.join(homedir, "dest")
    filenames = os.listdir(dest_dir)
    if (set(filenames) == set(fsizes.keys())):
        for f in filenames:
            if f != "thomas":
                try:
                    # can fail here if the symlink is messed up
                    size = path.getsize(path.join(dest_dir, f))
                except:
                    print("failed filesize get", f)
                    rsync_completed = False
                    break

                if fsizes[f] != size:
                    print("failed rsync size", f)
                    rsync_completed = False
                    break

            elif f == "thomas":
                try:
                    output = subprocess.check_output(
                        [path.join(dest_dir, f)], shell=True)
                except:
                    rsync_completed = False
                    break

                if "load average" not in str(output):
                    print("fail thomas test", str(output))
                    rsync_completed = False
                    break
            rsync_mtime = max(rsync_mtime, path.getmtime(
                path.join(dest_dir, f)))
    else:
        rsync_completed = False

    df.at[user, "rsync_completed"] = rsync_completed
    if rsync_completed:
        df.at[user, "rsync_completed_file_creation_time"] = rsync_mtime
# print(df)
print(df.to_csv())

import json

structure_path = "../documentation/curl_doc/doc.json"
details_path = "../documentation/curl_doc/curl-manpage-data.json"

with open(structure_path) as sreader:
    with open(details_path) as dreader:
        structure_data = json.load(sreader)
        details_data = json.load(dreader)

        et = structure_data["Establish a Transfer"]
        rr = et["Requests and Reponses"]
        option = rr[0]
        flag = option["flag"]
        sdesc = option["short-description"]
        print(flag + ":", sdesc)
        # ldesc = details_data[flag]
        # print(ldesc)

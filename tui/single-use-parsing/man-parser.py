import re
import json

filename = "rsync.txt"

with open(filename) as reader:
    lines = reader.readlines()
    inside_flag_desc = False
    data = {}
    for s in lines:
        if inside_flag_desc:
            m = re.match('(\s+)', s)
            if m:
                indent = len(m.group(0))

                if flag_indent != indent:
                    current_desc = current_desc + s + "\n"
                elif flag_indent == indent:
                    data[flag] = current_desc
                    inside_flag_desc = False

        if not inside_flag_desc:
            m = re.match('(\s+\-)', s)
            if m:
                flag_indent = len(m.group(0)) - 1
                flag = s.strip()
                inside_flag_desc = True
                current_desc = ""

    print(json.dumps(data))

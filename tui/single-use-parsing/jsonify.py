import getopt
import sys
import json

try:
    opts, args = getopt.getopt(sys.argv[1:], "", ["help", "output="])
except getopt.GetoptError as err:
    print(str(err))

f = args[0]

with open(f) as reader:
    print(json.dumps(reader.readlines()))

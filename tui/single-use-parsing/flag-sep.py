import json
import re


def main():
    fromf = "../documentation/rsync_doc/doc.json"

    with open(fromf) as reader:
        data = json.load(reader)
        print(json.dumps(transform(data)))


def transform(data):
    new_data = {}
    for k, v in data.items():
        if type(v) == list:
            new_list = []
            for line in v:
                line = line.strip()
                split_line = line.split(" ")
                sdesc = ""
                flag = ""
                for word in split_line:
                    if sdesc == "":
                        if re.match('-.*', word):
                            flag = flag + word + " "
                            continue
                        if re.match('<.*>', word):
                            flag = flag + word + " "
                            continue
                    sdesc = sdesc + word + " "
                flag = flag.strip()
                sdesc = sdesc.strip()
                new_list = new_list + \
                    [{"flag": flag, "short-description": sdesc}]
            new_data[k] = new_list
        else:
            new_data[k] = transform(v)
    return new_data


if __name__ == "__main__":
    main()

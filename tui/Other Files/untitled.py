from asciimatics.widgets import Frame, ListBox, Layout, Divider, Text, Button, TextBox, Widget, Label
from asciimatics.scene import Scene
from asciimatics.screen import Screen
from asciimatics.exceptions import ResizeScreenError, NextScene, StopApplication
import sys
import os
import pyperclip
import json

class LandingView(Frame):
	def __init__(self, screen, help_text=None):
		super(LandingView, self).__init__(screen,
											screen.height,
											screen.width,
											# on_load=self._reload_list,
											hover_focus=True,
											can_scroll=False,
											title="Help Page")

		
		# layout_label.add_widget(Divider())

		# layout_buttons.add_widget(Divider())
		self._help_label = Label(help_text)
		layout_help_label = Layout([100])
		self.add_layout(layout_help_label)
		layout_help_label.add_widget(self._help_label)
		layout_help_label.add_widget(Divider())
		
		#adding buttons
		self._continue_button = Button("Continue", self._continue)
		layout_buttons = Layout([50, 50])
		self.add_layout(layout_buttons)
		layout_buttons.add_widget(self._continue_button, 0)

		self._quit_button = Button("Quit", self._quit)
		layout_buttons.add_widget(self._quit_button, 1)

		#add summary
		custom_text = "rsync [OPTION...] SRC... [DEST]\n\n\nDescription\n\nRsync is a fast and versatile file copying tool. It is widely used for backups and mirroring and as an improved copy command for everyday use.\n\n\nUsage\n\nYou must specify a source and a destination. The following example transfers all files matching the pattern *.c from the current directory to the directory src on the machine foo:\n\nrsync -t *.c foo:src/\n\nIf any of the files already exist on the remote system then the rsync remote-update protocol is used to update the file by sending only the differences."
		self.data = {"usage": custom_text}
		
		self._label = TextBox(Widget.FILL_FRAME,"Usage" ,"usage", as_string=True, line_wrap=True)
		self._label.custom_colour = "selected_field"
		
		self._label.disabled = True
		layout_label = Layout([100])
		self.add_layout(layout_label)
		layout_label.add_widget(Divider())
		layout_label.add_widget(self._label)


		# fix layout
		self.fix()



	def _continue(self):
		raise NextScene("Flag Categories")
	@staticmethod
	def _quit():
	    raise StopApplication("User pressed quit")


def demo(screen, scene):
	scenes = [Scene(
				[LandingView(screen=screen, help_text="Navigate with ARROW KEYS and TAB")],
					-1,
					name="Landing")]

	screen.play(scenes, stop_on_resize=True, start_scene=scene, allow_int=True)

last_scene=None

while True:
    try:
        Screen.wrapper(demo, catch_interrupt=False, arguments=[last_scene])
        sys.exit(0)
    except ResizeScreenError as e:
        last_scene = e.scene

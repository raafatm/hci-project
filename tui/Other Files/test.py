import json

structure_path = "../documentation/curl_doc/doc.json"
details_path = "../documentation/curl_doc/curl-manpage-data.json"

class DataModel(object):
    def __init__(self, structure_path, details_path):
        # read in data
        with open(structure_path) as sreader:
            with open(details_path) as dreader:
                self.structure_data = json.load(sreader)
                self.details_data = json.load(dreader)

                self.categories = []
                self.sub_categories = []

                i = 1
                for key in self.structure_data.keys():
                    self.categories.append((key, i))
                    i = i + 1
                
                j = 1

                # for data in self.structure_data["Establish a Transfer"]:
                #     print (data)

                et = self.structure_data["Establish a Transfer"]
                rr = et["Requests and Reponses"]

                for val in rr:
                    print(val["flag"] + " : " + val["short-description"])


                



model = DataModel(structure_path, details_path)
# print(model.categories[0][0])
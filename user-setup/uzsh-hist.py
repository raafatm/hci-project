import subprocess
import os


with open('ups.newusers', 'r') as upsfile:
    
    ups = upsfile.readlines()
    for line in ups:
        uname = line.split(":")[0]
        subprocess.run("echo HISTFILE=~/.histfile >> " +
                       os.path.join("/home", uname, ".zshrc"), shell=True)

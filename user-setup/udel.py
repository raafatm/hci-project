from Crypto.Random import random
import subprocess
import os


with open('ups.newusers', 'r') as upsfile:
    ups = upsfile.readlines()
    for line in ups:
        uname = line.split(":")[0]
        subprocess.run(["userdel", uname])
        subprocess.run(["rm", "-rf", os.path.join("/home", uname)])

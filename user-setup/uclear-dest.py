import subprocess
import os


with open('ups.newusers', 'r') as upsfile:
    us = os.listdir("/srv/timing")

    ups = upsfile.readlines()
    for line in ups:
        uname = line.split(":")[0]
        if uname not in us:
            print(uname)
            subprocess.run("rm -f " + os.path.join("/home", uname, "dest/*"), shell=True)

import subprocess
import os
import tempfile


REPO_URL = "https://gitlab.ethz.ch/raafatm/hci-project.git"

with tempfile.TemporaryDirectory() as temp_dir:
    subprocess.run(["git", "clone", REPO_URL, temp_dir])

    with open('ups.newusers', 'r') as upsfile:
        ups = upsfile.readlines()
        for line in ups:
            uinfo = line.split(":")
            uname = uinfo[0]
            print(uname)
            home_dir = os.path.join("/home", uname)

            subprocess.run("rm -rf " + os.path.join(home_dir, "*"), shell=True)

            workflow_files = os.path.join(temp_dir, "cli-workflow", "*")
            subprocess.run("cp -r " + workflow_files + " " + home_dir,
                           shell=True)

            zshrc = os.path.join(temp_dir, "user-setup", ".zshrc")
            subprocess.run("cp " + zshrc + " " + home_dir,
                           shell=True)

            subprocess.run(["chown", "-R", uname + ":" + uname, home_dir])

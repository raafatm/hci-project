from Crypto.Random import random
import json
import os

word_file = "/usr/share/dict/cracklib-small"
WORDS = open(word_file).read().splitlines()
SEPARATORS = ["~", "-", "_", "@", "#"]

PF_LENGTH = 2
NUM_USERS = 2
NUM_TC = 1
ups = []
for user_num in range(NUM_USERS):

    uname = "user" + str(user_num)
    pwd = random.choice(SEPARATORS).join(
        [random.choice(WORDS) for i in range(PF_LENGTH)])

    # newusers requires the following format
    # pw_name:pw_passwd:pw_uid:pw_gid:pw_gecos:pw_dir:pw_shell
    if user_num < NUM_TC:
        shell = "zsh"
    else:
        shell = "bash"

    newusers = (uname, pwd, 1002 + user_num, 1002 + user_num,
                "", os.path.join("/home", uname), os.path.join("/bin", shell))
    newusers_string = ":".join([str(i) for i in newusers])
    ups = ups + [newusers_string]


with open('ups.newusers', 'w') as outfile:
    [outfile.write(line + "\n") for line in ups]

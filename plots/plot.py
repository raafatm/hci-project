import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

MERGED_DATA = "8-12-merged.csv"

df = pd.read_csv(MERGED_DATA)

fig, axes = plt.subplots()

bins = np.linspace(0, 1200, 6)
cc = df[df["curl_completed"]]
cnc = df[df["curl_completed"] == False]
cct = np.clip(cc["curl"], bins[0], bins[-1])
rc = df[df["rsync_completed"]]
rnc = df[df["rsync_completed"] == False]
rct = np.clip(rc["rsync"], bins[0], bins[-1])
axes.hist([cct, rct], bins=bins)
plt.title("Time Taken for Successful Task Completion: CURL and RSYNC")
plt.xlabel("Time Taken (s) NEEDS LAST BIN")
plt.ylabel("Frequency")
axes.legend(["CURL", "RSYNC"])
plt.xticks(bins)
# plt.savefig("cr_hist.png", bbox_inches='tight')
# plt.show()
axes.clear()

tcc = cc[cc["group"] == "TAB"]
bcc = cc[cc["group"] == "BASE"]
axes.hist([bcc["curl"], tcc["curl"]], bins=bins)
plt.title(
    "Time Taken for Successful CURL Task Completion: Baseline and Tab Completion")
plt.xlabel("Time Taken (s)")
plt.ylabel("Frequency")
plt.xticks(bins)
axes.legend(["Baseline", "Tab Completion"])
# plt.savefig("ctb_hist.png", bbox_inches='tight')
# plt.show()
axes.clear()

rctbl = df.groupby("rsync_completed").size()
cctbl = df.groupby("curl_completed").size()
# print(rctbl)
# print(cctbl)

aggcc = cc.mean()["curl"], cc.std()["curl"]
aggcnc = cnc.mean()["curl"], cnc.std()["curl"]
aggrc = rc.mean()["rsync"], rc.std()["rsync"]
aggrnc = rnc.mean()["rsync"], rnc.std()["rsync"]
print(aggcc, aggcnc)
print(aggrc, aggrnc)

taggcc = tcc.mean()["curl"], tcc.std()["curl"]
baggcc = bcc.mean()["curl"], bcc.std()["curl"]
# print(taggcc, baggcc)

gbr = df.groupby(
    ["How easily were you able to accomplish the rsync task?", "rsync_completed"]).size()
gbc = df.groupby(
    ["How easily were you able to accomplish the curl task?", "curl_completed"]).size()
# print(gbr)
# print(gbc)

gbcer = rc.groupby(
    "How easily were you able to accomplish the rsync task?").size()
gbter = df.groupby(
    "How easily were you able to accomplish the rsync task?").size()
rease = (gbcer / gbter).fillna(value=0)
rease.plot()


gbcec = cc.groupby(
    "How easily were you able to accomplish the curl task?").size()
gbtec = df.groupby(
    "How easily were you able to accomplish the curl task?").size()
cease = (gbcec / gbtec).fillna(value=0)
cease.plot()
plt.title("Success Rate vs. Self Reported Ease of Completion")
plt.ylabel("Success Rate")
plt.xlabel("""Ease of Completion (1 is "Very Easy" and 5 is "Very Difficult")""")
plt.xticks(range(1, 6))
axes.legend(["RSYNC", "CURL"])
# plt.savefig("ease.png", bbox_inches='tight')
# plt.show()
axes.clear()

# This data undermines the hypothesis that tab-completion was actually helpful
t = df.groupby(
    "Select all that apply, choose the tools that helped you the most?").size()
# print(t)


# 5's were overconfident
gbcpc = cc.groupby("How comfortable are you with command line tools?").size()
gbtp = df.groupby("How comfortable are you with command line tools?").size()
cprof = (gbcpc / gbtp).fillna(value=0)
cprof.plot()

gbcpr = rc.groupby("How comfortable are you with command line tools?").size()
rprof = (gbcpr / gbtp).fillna(value=0)
rprof.plot()
plt.title("Success Rate vs. Self Reported CL Proficiency")
plt.ylabel("Success Rate")
plt.xlabel(
    """Comfort With Command Line (1 is "Very Uncomfortable" - 5 "Very Comfortable")""")
plt.xticks(range(1, 6))
plt.yticks(np.linspace(0, 1, 6))
axes.legend(["RSYNC", "CURL"])
# plt.savefig("prof.png", bbox_inches='tight')
# plt.show()
# print(rprof)
axes.clear()


# Maybe helped for curl
gbfrc = rc.groupby("How familiar are you with rsync?").size()
gbfrt = df.groupby("How familiar are you with rsync?").size()
rfam = (gbfrc / gbfrt).fillna(value=0)

gbfcc = cc.groupby("How familiar are you with curl?").size()
gbfct = df.groupby("How familiar are you with curl?").size()
cfam = (gbfcc / gbfct).fillna(value=0)
# print(rfam)
# print(cfam)
cfam.plot()
plt.xticks(np.linspace(1, 5, 5))
plt.yticks(np.linspace(0, 1, 6))
plt.title("Completion Rate of CURL Task vs. Familiarity With CURL")
plt.ylabel("Completion Rate")
# plt.savefig("fam.png", bbox_inches='tight')
# plt.show()
axes.clear()

grbr = df.groupby(
    ["How familiar are you with rsync?", "rsync_completed"]).size()
gcbc = df.groupby(
    ["How familiar are you with curl?", "curl_completed"]).size()
# print(gcbc)
# print(grbr)


spr = df.groupby(
    ["Study program", "rsync_completed"]).size()
spc = df.groupby(
    ["Study program", "curl_completed"]).size()
spr = rc.groupby("Study program").size()
sp = df.groupby("Study program").size()
sprcomp = (spr / sp).fillna(value=0)
spc = cc.groupby("Study program").size()
spccomp = (spc / sp).fillna(value=0)
# print(sprcomp)
# print(spr)
# print(spc)

spcomf = df.groupby(
    ["Study program", "How comfortable are you with command line tools?"]).size()
# print(spcomf)

bins = np.linspace(0, 1200, 6)

axes.hist([rnc["rsync"], rc["rsync"]], bins=bins, color=["r", "g"])
plt.title("Time taken for Successful and Unsuccessful RSYNC Task Completion")

plt.xticks(bins)
plt.yticks(np.linspace(0, 8, 9))
xlabels = [str(int(bin)) for bin in bins]
xlabels[-1] = ">" + xlabels[-2]
axes.set_xticklabels(xlabels)
plt.xlabel("Time Taken (s)")
plt.ylabel("Frequency")
axes.legend(["Not Successful", "Successful"])
# plt.savefig("r_hist.png", bbox_inches='tight')
# plt.show()
axes.clear()


cnct = np.clip(cnc["curl"], bins[0], bins[-1])
axes.hist([cnct, cct], bins=bins, color=["r", "g"])
plt.yticks(np.linspace(0, 8, 9))
plt.xticks(bins)
xlabels = [str(int(bin)) for bin in bins]
xlabels[-1] = ">" + xlabels[-2]
axes.set_xticklabels(xlabels)
plt.title("Time taken for Successful and Unsuccessful CURL Task Completion")
plt.xlabel("Time Taken (s)")
plt.ylabel("Frequency")
axes.legend(["Not Successful", "Successful"])
# plt.savefig("c_hist.png", bbox_inches='tight')
# plt.show()

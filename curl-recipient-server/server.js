var fs = require("fs")
var https = require("https")
var http = require("http")
var express = require("express")
const { logger } = require("./logger.js")

const ENV = "PROD"
// const ENV = "TEST"
logger.info("Starting in env " + ENV)

var KEYFILE = "certs/server-crt.pem"
var CERTFILE = "certs/server-key.pem"
if (ENV == "PROD") {
	const LETS_ENC_CERT_DIR = "/etc/letsencrypt/live/cli-experiment.ddns.net/"
	KEYFILE = LETS_ENC_CERT_DIR + "privkey.pem"
	CERTFILE = LETS_ENC_CERT_DIR + "fullchain.pem"
}

var options = { 
    key: fs.readFileSync(KEYFILE), 
    cert: fs.readFileSync(CERTFILE), 
    ca: fs.readFileSync("certs/ca-crt.pem"), 
    requestCert: true,
    rejectUnauthorized: true,
}


const app = express()

app.get("/*", (req, res) => {
    if (!req.client.authorized) {
        res
            .status(503)
            .send("PLEASE USE THE HTTPS ADDRESS BELOW:\n https://" + req.headers.host + req.url + "\n")
        return
    }
    res.send("The secret password is: pineapple\n")
})

const HTTPS_PORT= 443
const HTTP_PORT= 80
https.createServer(options, app).listen(HTTPS_PORT)
logger.info("listening HTTPS on port " + HTTPS_PORT)
http.createServer(options, app).listen(HTTP_PORT)
logger.info("listening HTTP on port " + HTTP_PORT)

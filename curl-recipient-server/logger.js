const { createLogger, format, transports } = require("winston")
const { json, printf, combine, timestamp} = format

const logger = createLogger({
    level: "info",
    format: combine(
        timestamp(),
        json(),
        printf(info => {
            return `${info.timestamp} [${info.level}]: ${info.message}`
        })
    ),
    transports: [
        new transports.Console()
    ]
})

module.exports = {
    logger: logger
}


# CLI UX Study Instructions
This is a study to evaluate how design choices affect command line interface (CLI)
usability. For this study you will complete two tasks on a remote server, and
answer some short survey questions. Please don't feel the need to spend more
than 10 minutes attempting to complete the tasks. Both can be completed with a
single command and a few options.  Please **DO NOT** skip back and forth
between tasks. Complete the first task before moving onto the second. Thanks
for your participation!

To complete the tasks you are free to use any resources you'd like (including
the internet!).

### Task 0: Logging in

To open a terminal on Mac, start the Terminal utility. On Windows, open up
PowerShell.

To log-in to the remote server, please use the following command (type it into
a terminal and press enter):

```ssh <username>@cli-experiment.ddns.net```

where ```<username>``` is replaced with your assigned username (something like
```user14```). Type in your assigned passphrase at the prompt.

## Command Line Basics & Man Pages
Feel free to skip ahead if you have some experience with the Linux command
line or with man pages. If you need a refresh, please have a look at the bottom of this page!

# TASK 1: Pre-work Survey (~30s)

Please fill out the **FIRST PAGE ONLY** of the survey at:

[https://forms.gle/JtZzze43Yz5JwB667](https://forms.gle/JtZzze43Yz5JwB667)

You will fill out the second page after completing tasks 1 and 2.

# TASK 2: RSYNC

Please read all of the instructions **BEFORE** starting the task

Complete all steps in the terminal you used in TASK 0 to log into the remote server

1. Execute ```start_rsync_task``` so that we have information on how long the
   task takes you

2. Copy the files specified in ```poets.txt``` from the ```src``` directory to
   the ```dest``` directory using the ```rsync``` command with the following
   restrictions:

    a. Copy only files less than 70 Kilobytes in size

    b. Some of the files are actually shortcuts (symbolic links) to other
       files. Make sure that these shortcuts still work after the files are
       copied

3. Execute ```end_rsync_task``` when you are finished

If you are unable to complete the task, you can just perform step 3 and
move on at any time. Just please do not revisit the task after executing the
end task command (step 3).

# TASK 3: CURL

Please read all of the instructions **BEFORE** starting the task

Complete all steps in the terminal you used in TASK 0 to log into the remote server

1. Execute ```start_curl_task```

2. Use ```curl``` to make an HTTPS GET request to
   ```https://cli-experiment.ddns.net```

    a. Use the PEM files provided to authenticate to the server with a client
       certificate

    b. Store the response from the server in an output file named
       ```curl_response.html```

3. Execute ```end_curl_task```

If you are unable to complete the task, you can just perform step 3 and
move on at any time. Just please do not revisit the task after executing the
end task command (step 3).

# TASK 4: Post-work Survey (~30s)

Please fill out the second page of the survey you began in TASK 1

Thanks again for participating!

## Command Line Basics & Man Pages
The following part is a small refresh about command line basics and man pages.
If you already completed the tasks, please ignore this section.

### Command Line Basics
Command line applications in Linux are typically characterized by a base
command, some options/flags, and then some operands.

For example, to see all of the files in a given folder, I can use the ```ls```
command. By default, just typing ```ls``` without flags or operands will list
the files in the current folder. But, if I add the ```src``` folder as an
operand (i.e. ```ls src```) it will list all the files in the ```src``` folder
instead.

Flags/options typically come in pairs of the flag/option itself and a value for
that option. The flags are usually preceeded by either ```--``` or ```-```. For
example, to see a more detailed list of the files in the ```src``` folder, I
can enter ```ls -l src```. In this case, the ```-l``` flag has no value . But
other flags will need values. For example, to change the format of ```ls``` to
include commas, we can use the ```--format``` option, but we also need to pass a
value to it. In this case, we execute ```ls --format commas src``` to see
the files in ```src``` separated by commas. You can combine multiple flags and
values together by adding them sequentially to your command before the
operand(s), being sure to keep values directly after their corresponding options.

### Man Pages
As a reminder, you are **NOT** required to use the man pages at all to complete the tasks.

To see the man pages (documentation) for a program, you just need to execute
```man <program>```, where ```<program>``` is the name of the application you
want to see the documentation for. For example, you can execute ```man
ls``` to see the documentation for the ```ls``` program). Manual pages are
navigable with the arrow keys, and can be searched by pressing ```/``` before
typing a search term. Search results are highlighted and can be cycled through
by pressing ```n```.  To exit the manual, press ```q```.
